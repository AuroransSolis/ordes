use crate::{OrdesCons, OrdesPop, OrdesPush};
use ::std::net::{IpAddr, Ipv4Addr, Ipv6Addr};

#[test]
fn test_push_arr() {
    let foo = [0, 1, 2, 3, 4];
    let bar = foo.push(5);
    assert_eq!(bar, [0, 1, 2, 3, 4, 5]);
}

#[test]
fn test_push_tuple() {
    let foo = ('a', 10, false);
    let bar = foo.push(["hello", "world"]);
    assert_eq!(bar, ('a', 10, false, ["hello", "world"]));
}

#[test]
fn test_pop_arr() {
    let foo = [0, 1, 2, 3, 4];
    let (bar, popped) = foo.pop();
    assert_eq!(bar, [0, 1, 2, 3]);
    assert_eq!(popped, 4);
}

#[test]
fn test_pop_tuple() {
    let foo = ('a', 10, false, ["hello", "world"]);
    let (bar, popped) = foo.pop();
    assert_eq!(bar, ('a', 10, false));
    assert_eq!(popped, ["hello", "world"]);
}

const UD: [char; 2] = ['↑', '↓'];
const LR: [char; 2] = ['←', '→'];

#[test]
fn test_loss_arr() {
    UD.iter()
        .flat_map(|&a| UD.iter().map(move |&b| [a, b]))
        .flat_map(|arr| UD.iter().map(move |&c| arr.push(c)))
        .flat_map(|arr| UD.iter().map(move |&d| arr.push(d)))
        .flat_map(|arr| UD.iter().map(move |&e| arr.push(e)))
        .flat_map(|arr| UD.iter().map(move |&f| arr.push(f)))
        .flat_map(|arr| LR.iter().map(move |&g| arr.push(g)))
        .for_each(|[a, b, c, d, e, f, g]| println!("{} {}{} {}{} {}{}", a, b, c, d, e, f, g));
}

#[test]
fn test_loss_tuple() {
    UD.iter()
        .flat_map(|&a| UD.iter().map(move |&b| (a, b)))
        .flat_map(|chars| UD.iter().map(move |&c| chars.push(c)))
        .flat_map(|chars| UD.iter().map(move |&d| chars.push(d)))
        .flat_map(|chars| UD.iter().map(move |&e| chars.push(e)))
        .flat_map(|chars| UD.iter().map(move |&f| chars.push(f)))
        .flat_map(|chars| LR.iter().map(move |&g| chars.push(g)))
        .for_each(|(a, b, c, d, e, f, g)| println!("{} {}{} {}{} {}{}", a, b, c, d, e, f, g));
}

#[test]
fn test_ipaddr_bytestream() {
    let addrs = [
        IpAddr::V4(Ipv4Addr::from([0, 1, 2, 3])),
        IpAddr::V6(Ipv6Addr::from([
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
        ])),
    ];
    let addr_bytes: [&[u8]; 2] = [
        &[4, 0, 1, 2, 3],
        &[6, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    ];
    for (addr, bytes) in addrs.into_iter().zip(addr_bytes.into_iter()) {
        for (test, &confirm) in ipaddr_bytestream(addr).zip(bytes.iter()) {
            assert_eq!(test, confirm);
        }
    }
}

fn ipaddr_bytestream(addr: IpAddr) -> impl Iterator<Item = u8> {
    let mut data = [0; 17];
    let len = match addr {
        IpAddr::V4(addr) => {
            data[0..5].copy_from_slice(&addr.octets().cons(4));
            5
        }
        IpAddr::V6(addr) => {
            data[0..17].copy_from_slice(&addr.octets().cons(6));
            17
        }
    };
    data.into_iter().take(len)
}
