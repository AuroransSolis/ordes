#![cfg_attr(feature = "const_generics", allow(incomplete_features))]
#![cfg_attr(feature = "const_generics", feature(generic_const_exprs))]
#![cfg_attr(
    feature = "const_generics",
    doc(test(attr(allow(incomplete_features))))
)]
#![cfg_attr(
    feature = "const_generics",
    doc(test(attr(feature(generic_const_exprs))))
)]

#[allow(dead_code)]
#[cfg(test)]
mod tests;

#[cfg(not(feature = "const_generics"))]
mod stable_arr;

#[cfg(feature = "const_generics")]
mod nightly_arr;

#[cfg(feature = "const_generics")]
pub use nightly_arr::*;

/// Trait for defining the removal of the last element from an array- or tuple-like data structure.
pub trait OrdesPop {
    /// Type of the container after `.pop()` has been called.
    type Newlen;
    /// Type of the removed value.
    type Output;

    /// Remove the last element from `self` and return it.
    /// # Examples
    /// Using an array:
    /// ```
    /// # use ordes::OrdesPop;
    /// let foo: [u8; 5] = [0, 1, 2, 3, 4];
    /// let (foo, pop) = foo.pop();
    /// assert_eq!(foo, [0, 1, 2, 3]);
    /// assert_eq!(pop, 4);
    /// ```
    /// Using a tuple:
    /// ```
    /// # use ordes::OrdesPop;
    /// let foo = ('a', false, b'c', "d");
    /// let (foo, pop) = foo.pop();
    /// assert_eq!(foo, ('a', false, b'c'));
    /// assert_eq!(pop, "d");
    /// ```
    fn pop(self) -> (Self::Newlen, Self::Output);
}

// Consider adding remove?

/// Trait for defining the removal of the first element from an array- or tuple-like data structure.
pub trait OrdesRest {
    /// Type of the container after `.rest()` has been called.
    type Newlen;
    /// Type of the removed value.
    type Output;

    /// Removes the first element from `self` and returns it.
    /// # Examples
    /// Using an array:
    /// ```
    /// # use ordes::OrdesRest;
    /// let foo: [u8; 5] = [0, 1, 2, 3, 4];
    /// let (foo, pop) = foo.rest();
    /// assert_eq!(foo, [1, 2, 3, 4]);
    /// assert_eq!(pop, 0);
    /// ```
    /// Using a tuple:
    /// ```
    /// # use ordes::OrdesRest;
    /// let foo = ('a', false, b'c', "d");
    /// let (foo, pop) = foo.rest();
    /// assert_eq!(foo, (false, b'c', "d"));
    /// assert_eq!(pop, 'a');
    /// ```
    fn rest(self) -> (Self::Newlen, Self::Output);
}

/// Trait for defining pushing to an array- or tuple-like data structure.
pub trait OrdesPush<Input> {
    type Output;

    /// Add an element to the end of `self` and return the sum.
    /// # Examples
    /// Using an array:
    /// ```
    /// # use ordes::OrdesPush;
    /// let foo: [u8; 4] = [0, 1, 2, 3];
    /// let foo = foo.push(4);
    /// assert_eq!(foo, [0, 1, 2, 3, 4]);
    /// ```
    /// Using a tuple:
    /// ```
    /// # use ordes::OrdesPush;
    /// let foo = ('a', false, b'c');
    /// let foo = foo.push("d");
    /// assert_eq!(foo, ('a', false, b'c', "d"));
    /// ```
    fn push(self, value: Input) -> Self::Output;

    // Consider adding insert?
}

pub trait OrdesCons<Input> {
    type Output;

    /// Add an element to the start of `self` and return the sum.
    /// # Examples
    /// Using an array:
    /// ```
    /// # use ordes::OrdesCons;
    /// let foo: [u8; 4] = [1, 2, 3, 4];
    /// let foo = foo.cons(0);
    /// assert_eq!(foo, [0, 1, 2, 3, 4]);
    /// ```
    /// Using a tuple:
    /// ```
    /// # use ordes::OrdesCons;
    /// let foo = (false, b'c', "d");
    /// let foo = foo.cons('a');
    /// assert_eq!(foo, ('a', false, b'c', "d"));
    /// ```
    fn cons(self, value: Input) -> Self::Output;
}

use ordes_macros::impl_ops_tuple;
use seq_macro::seq;

seq! {N in 1..=32 {
    impl_ops_tuple!(N);
}}

#[cfg(feature = "len_32_64")]
seq! {N in 33..=64 {
    impl_ops_tuple!(N);
}}

#[cfg(feature = "len_64_128")]
seq! {N in 65..=128 {
    impl_ops_tuple!(N);
}}

#[cfg(feature = "len_128_256")]
seq! {N in 129..=256 {
    impl_ops_tuple!(N);
}}

#[cfg(feature = "len_256_512")]
seq! {N in 257..=512 {
    impl_ops_tuple!(N);
}}

#[cfg(feature = "len_512_1024")]
seq! {N in 513..=1024 {
    impl_ops_tuple!(N);
}}
