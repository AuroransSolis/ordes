use crate::{OrdesCons, OrdesPop, OrdesPush, OrdesRest};
use ::core::mem::ManuallyDrop;

pub struct ConstCheck<const CHECK: bool>;

pub trait True {}
impl True for ConstCheck<true> {}

pub trait IsArray {
    type Inner;
    const LEN: usize;
}

impl<T, const N: usize> IsArray for [T; N] {
    type Inner = T;
    const LEN: usize = N;
}

#[repr(C)]
struct PushJoin<T, const N: usize> {
    arr: [T; N],
    last: T,
}

#[repr(C)]
union PushJoiner<T, const N: usize>
where
    [(); N + 1]:,
{
    whole: ManuallyDrop<[T; N + 1]>,
    split: ManuallyDrop<PushJoin<T, N>>,
}

impl<T, const N: usize> OrdesPush<T> for [T; N]
where
    ConstCheck<{ N < ::core::usize::MAX }>: True,
    [T; N + 1]:,
{
    type Output = [T; N + 1];

    fn push(self, input: T) -> Self::Output {
        unsafe {
            let joiner = PushJoiner {
                split: ManuallyDrop::new(PushJoin {
                    arr: self,
                    last: input,
                }),
            };
            let join = joiner.whole;
            ManuallyDrop::into_inner(join)
        }
    }
}

#[repr(C)]
struct ConsJoin<T, const N: usize> {
    first: T,
    arr: [T; N],
}

#[repr(C)]
union ConsJoiner<T, const N: usize>
where
    [(); N + 1]:,
{
    whole: ManuallyDrop<[T; N + 1]>,
    split: ManuallyDrop<ConsJoin<T, N>>,
}

impl<T, const N: usize> OrdesCons<T> for [T; N]
where
    ConstCheck<{ N < ::core::usize::MAX }>: True,
    [T; N + 1]:,
{
    type Output = [T; N + 1];

    fn cons(self, input: T) -> Self::Output {
        unsafe {
            let joiner = ConsJoiner {
                split: ManuallyDrop::new(ConsJoin {
                    first: input,
                    arr: self,
                }),
            };
            let join = joiner.whole;
            ManuallyDrop::into_inner(join)
        }
    }
}

#[repr(C)]
struct PopSplit<T, const N: usize>
where
    [(); N - 1]:,
{
    arr: [T; N - 1],
    last: T,
}

#[repr(C)]
union PopSplitter<T, const N: usize>
where
    [(); N - 1]:,
{
    whole: ManuallyDrop<[T; N]>,
    split: ManuallyDrop<PopSplit<T, N>>,
}

impl<T, const N: usize> OrdesPop for [T; N]
where
    ConstCheck<{ N > 0 }>: True,
    [T; N - 1]:,
{
    type Newlen = [T; N - 1];
    type Output = T;

    fn pop(self) -> (Self::Newlen, Self::Output) {
        unsafe {
            let splitter = PopSplitter {
                whole: ManuallyDrop::new(self),
            };
            let split = splitter.split;
            let PopSplit { arr, last } = ManuallyDrop::into_inner(split);
            (arr, last)
        }
    }
}

#[repr(C)]
struct RestSplit<T, const N: usize>
where
    [(); N - 1]:,
{
    first: T,
    arr: [T; N - 1],
}

#[repr(C)]
union RestSplitter<T, const N: usize>
where
    [(); N]:,
    [(); N - 1]:,
{
    whole: ManuallyDrop<[T; N]>,
    split: ManuallyDrop<RestSplit<T, N>>,
}

impl<T, const N: usize> OrdesRest for [T; N]
where
    ConstCheck<{ N > 0 }>: True,
    [T; N - 1]:,
{
    type Newlen = [T; N - 1];
    type Output = T;

    fn rest(self) -> (Self::Newlen, Self::Output) {
        unsafe {
            let splitter = RestSplitter {
                whole: ManuallyDrop::new(self),
            };
            let split = splitter.split;
            let RestSplit { first, arr } = ManuallyDrop::into_inner(split);
            (arr, first)
        }
    }
}

/// Trait for defining splitting an array into two owned arrays at a constant length.
pub trait OrdesSplit: IsArray {
    /// The first item in the returned tuple contains the first `I` elements of the original array,
    /// and the second item contains the rest of the original array.
    /// # Example
    /// ```
    /// # use ordes::OrdesSplit;
    /// let foo = [0, 1, 2, 3, 4, 5];
    /// let (foo, bar) = foo.ordes_split::<3>();
    /// assert_eq!(foo, [0, 1, 2]);
    /// assert_eq!(bar, [3, 4, 5]);
    /// ```
    fn ordes_split<const I: usize>(self) -> ([Self::Inner; I], [Self::Inner; Self::LEN - I])
    where
        ConstCheck<{ I <= Self::LEN }>: True,
        [(); I]:,
        [(); Self::LEN - I]:;
}

#[repr(C)]
struct SplitSplit<T, const N: usize, const I: usize>
where
    [(); I]:,
    [(); <[T; N] as IsArray>::LEN - I]:,
{
    left: [T; I],
    right: [T; <[T; N] as IsArray>::LEN - I],
}

#[repr(C)]
union SplitSplitter<T, const N: usize, const I: usize>
where
    [(); I]:,
    [(); <[T; N] as IsArray>::LEN - I]:,
{
    whole: ManuallyDrop<[T; N]>,
    split: ManuallyDrop<SplitSplit<T, N, I>>,
}

impl<T, const N: usize> OrdesSplit for [T; N]
where
    ConstCheck<{ N > 0 }>: True,
{
    fn ordes_split<const I: usize>(self) -> ([T; I], [T; Self::LEN - I])
    where
        ConstCheck<{ I <= Self::LEN }>: True,
        [(); I]:,
        [(); Self::LEN - I]:,
    {
        unsafe {
            let splitter: SplitSplitter<T, N, I> = SplitSplitter {
                whole: ManuallyDrop::new(self),
            };
            let split = splitter.split;
            let SplitSplit { left, right } = ManuallyDrop::into_inner(split);
            (left, right)
        }
    }
}

/// Trait for defining the concatenation of two arrays.
pub trait OrdesConcat<U> {
    /// Output type of the `.concat()` method.
    type Output;

    /// Concatenates two arrays into a single one.
    /// # Example
    /// ```
    /// # use ordes::OrdesConcat;
    /// let foo = ["H", "e"];
    /// let bar = ["l", "l", "o"];
    /// let baz = foo.concatenate(bar);
    /// assert_eq!(baz, ["H", "e", "l", "l", "o"]);
    /// ```
    fn concatenate(self, other: U) -> Self::Output;
}

#[repr(C)]
struct ConcatJoin<T, const N: usize, const M: usize> {
    left: [T; N],
    right: [T; M],
}

#[repr(C)]
union ConcatJoiner<T, const N: usize, const M: usize>
where
    [(); N + M]:,
{
    whole: ManuallyDrop<[T; N + M]>,
    split: ManuallyDrop<ConcatJoin<T, N, M>>,
}

impl<T, const N: usize, const M: usize> OrdesConcat<[T; M]> for [T; N]
where
    ConstCheck<{ N <= ::core::usize::MAX - M }>: True,
    [(); N + M]:,
{
    type Output = [T; N + M];

    fn concatenate(self, other: [T; M]) -> Self::Output {
        unsafe {
            let joiner = ConcatJoiner {
                split: ManuallyDrop::new(ConcatJoin {
                    left: self,
                    right: other,
                }),
            };
            let join = joiner.whole;
            ManuallyDrop::into_inner(join)
        }
    }
}

/// Trait for defining the removal of an arbitrary element from an array.
pub trait OrdesRemove: IsArray {
    /// Remove the `I`th index of the array.
    /// # Example
    /// ```
    /// # use ordes::OrdesRemove;
    /// let foo = ["H", "e", "m", "l", "l", "o"];
    /// let (foo, rem) = foo.remove::<2>();
    /// assert_eq!(foo, ["H", "e", "l", "l", "o"]);
    /// assert_eq!(rem, "m");
    /// ```
    fn remove<const I: usize>(self) -> ([Self::Inner; Self::LEN - 1], Self::Inner)
    where
        ConstCheck<{ I < Self::LEN }>: True,
        [(); Self::LEN - I - 1]:;
}

#[repr(C)]
struct RemoveSplit<T, const N: usize, const I: usize>
where
    [(); <[T; N] as IsArray>::LEN - I - 1]:,
{
    left: [T; I],
    remove: T,
    right: [T; <[T; N] as IsArray>::LEN - I - 1],
}

#[repr(C)]
union RemoveSplitter<T, const N: usize, const I: usize>
where
    [(); <[T; N] as IsArray>::LEN - I - 1]:,
{
    whole: ManuallyDrop<[T; N]>,
    split: ManuallyDrop<RemoveSplit<T, N, I>>,
}

#[repr(C)]
struct RemoveJoin<T, const N: usize, const I: usize>
where
    [(); <[T; N] as IsArray>::LEN - I - 1]:,
{
    left: [T; I],
    right: [T; <[T; N] as IsArray>::LEN - I - 1],
}

#[repr(C)]
union RemoveJoiner<T, const N: usize, const I: usize>
where
    [(); <[T; N] as IsArray>::LEN - 1]:,
    [(); <[T; N] as IsArray>::LEN - I - 1]:,
{
    whole: ManuallyDrop<[T; <[T; N] as IsArray>::LEN - 1]>,
    split: ManuallyDrop<RemoveJoin<T, N, I>>,
}

impl<T, const N: usize> OrdesRemove for [T; N]
where
    ConstCheck<{ N > 0 }>: True,
    [(); Self::LEN - 1]:,
{
    fn remove<const I: usize>(self) -> ([T; Self::LEN - 1], Self::Inner)
    where
        ConstCheck<{ I < Self::LEN }>: True,
        [(); Self::LEN - I - 1]:,
    {
        unsafe {
            let splitter: RemoveSplitter<T, N, I> = RemoveSplitter {
                whole: ManuallyDrop::new(self),
            };
            let split = splitter.split;
            let RemoveSplit {
                left,
                remove,
                right,
            } = ManuallyDrop::into_inner(split);
            let joiner: RemoveJoiner<T, N, I> = RemoveJoiner {
                split: ManuallyDrop::new(RemoveJoin { left, right }),
            };
            let join = joiner.whole;
            (ManuallyDrop::into_inner(join), remove)
        }
    }
}

/// Trait for defining inserting an item into an array.
pub trait OrdesInsert: IsArray {
    /// Insert an item into the `I`th index of the array.
    /// # Example
    /// ```
    /// # use ordes::OrdesInsert;
    /// let foo = ["H", "e", "l", "l", "o", " ", "w", "o", "r", "l", "d", "!"];
    /// let foo = foo.insert::<5>(",");
    /// assert_eq!(foo, ["H", "e", "l", "l", "o", ",", " ", "w", "o", "r", "l", "d", "!"]);
    /// ```
    fn insert<const I: usize>(self, input: Self::Inner) -> [Self::Inner; Self::LEN + 1]
    where
        ConstCheck<{ I < Self::LEN }>: True,
        [(); Self::LEN - I]:;
}

#[repr(C)]
struct InsertSplit<T, const N: usize, const I: usize>
where
    [(); <[T; N] as IsArray>::LEN - I]:,
{
    first: [T; I],
    second: [T; <[T; N] as IsArray>::LEN - I],
}

#[repr(C)]
union InsertSplitter<T, const N: usize, const I: usize>
where
    [(); <[T; N] as IsArray>::LEN - I]:,
{
    whole: ManuallyDrop<[T; N]>,
    split: ManuallyDrop<InsertSplit<T, N, I>>,
}

#[repr(C)]
struct InsertJoin<T, const N: usize, const I: usize>
where
    [(); <[T; N] as IsArray>::LEN - I]:,
{
    first: [T; I],
    insert: T,
    second: [T; <[T; N] as IsArray>::LEN - I],
}

#[repr(C)]
union InsertJoiner<T, const N: usize, const I: usize>
where
    [(); <[T; N] as IsArray>::LEN + 1]:,
    [(); <[T; N] as IsArray>::LEN - I]:,
{
    whole: ManuallyDrop<[T; <[T; N] as IsArray>::LEN + 1]>,
    split: ManuallyDrop<InsertJoin<T, N, I>>,
}

impl<T, const N: usize> OrdesInsert for [T; N]
where
    ConstCheck<{ Self::LEN < ::core::usize::MAX }>: True,
    [(); Self::LEN + 1]:,
{
    fn insert<const I: usize>(self, input: Self::Inner) -> [Self::Inner; Self::LEN + 1]
    where
        ConstCheck<{ I < Self::LEN }>: True,
        [(); Self::LEN - I]:,
    {
        unsafe {
            let splitter: InsertSplitter<T, N, I> = InsertSplitter {
                whole: ManuallyDrop::new(self),
            };
            let split = splitter.split;
            let InsertSplit { first, second } = ManuallyDrop::into_inner(split);
            let joiner: InsertJoiner<T, N, I> = InsertJoiner {
                split: ManuallyDrop::new(InsertJoin {
                    first,
                    insert: input,
                    second,
                }),
            };
            let join = joiner.whole;
            ManuallyDrop::into_inner(join)
        }
    }
}

/// Trait for breaking up an array into multiple sub-arrays.
pub trait OrdesChunks: IsArray {
    /// Break up an array `N` long into chunks of length `M`.
    /// # Example
    /// ```
    /// # use ordes::OrdesChunks;
    /// let foo = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    /// let (chunks, leftover) = foo.ordes_chunks::<4>();
    /// assert_eq!(chunks, [[0, 1, 2, 3], [4, 5, 6, 7]]);
    /// assert_eq!(leftover, [8, 9, 10]);
    /// ```
    fn ordes_chunks<const M: usize>(
        self,
    ) -> (
        [[Self::Inner; M]; Self::LEN / M],
        [Self::Inner; Self::LEN % M],
    )
    where
        ConstCheck<{ M > 0 }>: True,
        ConstCheck<{ M <= Self::LEN }>: True,
        [(); M]:,
        [(); Self::LEN / M]:,
        [(); Self::LEN % M]:;
}

#[repr(C)]
struct ChunkSplit<T, const M: usize, const N: usize>
where
    ConstCheck<{ M > 0 }>: True,
    ConstCheck<{ M <= <[T; N] as IsArray>::LEN }>: True,
    [(); M]:,
    [(); <[T; N] as IsArray>::LEN / M]:,
    [(); <[T; N] as IsArray>::LEN % M]:,
{
    left: [[T; M]; <[T; N] as IsArray>::LEN / M],
    right: [T; <[T; N] as IsArray>::LEN % M],
}

#[repr(C)]
union ChunkSplitter<T, const M: usize, const N: usize>
where
    ConstCheck<{ M > 0 }>: True,
    ConstCheck<{ M <= <[T; N] as IsArray>::LEN }>: True,
    [(); M]:,
    [(); <[T; N] as IsArray>::LEN / M]:,
    [(); <[T; N] as IsArray>::LEN % M]:,
{
    whole: ManuallyDrop<[T; N]>,
    chunks: ManuallyDrop<ChunkSplit<T, M, N>>,
}

impl<T, const N: usize> OrdesChunks for [T; N] {
    fn ordes_chunks<const M: usize>(self) -> ([[T; M]; Self::LEN / M], [T; Self::LEN % M])
    where
        ConstCheck<{ M > 0 }>: True,
        ConstCheck<{ M <= Self::LEN }>: True,
        [(); M]:,
        [(); Self::LEN / M]:,
        [(); Self::LEN % M]:,
    {
        unsafe {
            let splitter: ChunkSplitter<T, M, N> = ChunkSplitter {
                whole: ManuallyDrop::new(self),
            };
            let chunks = splitter.chunks;
            let ChunkSplit { left, right } = ManuallyDrop::into_inner(chunks);
            (left, right)
        }
    }
}

/// Trait for flattening `[[T; M]; N]` into `[T; M * N]`.
pub trait OrdesFlatten: IsArray<Inner: IsArray>
where
    ConstCheck<{ <Self::Inner as IsArray>::LEN <= ::core::usize::MAX / Self::LEN }>: True,
    ConstCheck<{ Self::LEN <= ::core::usize::MAX / <Self::Inner as IsArray>::LEN }>: True,
    [(); Self::LEN * <Self::Inner as IsArray>::LEN]:,
{
    /// Flattens out a `[[T; M]; N]` into a `[T; M * N]`.
    /// # Example
    /// ```
    /// # use ordes::OrdesFlatten;
    /// let foo = [[0, 1, 2], [3, 4, 5], [6, 7, 8]];
    /// let flat = foo.flatten();
    /// assert_eq!(flat, [0, 1, 2, 3, 4, 5, 6, 7, 8]);
    /// ```
    fn flatten(
        self,
    ) -> [<Self::Inner as IsArray>::Inner; Self::LEN * <Self::Inner as IsArray>::LEN];
}

#[repr(C)]
union FlattenJoiner<T>
where
    T: IsArray,
    T::Inner: IsArray,
    ConstCheck<{ <T::Inner as IsArray>::LEN <= ::core::usize::MAX / T::LEN }>: True,
    ConstCheck<{ T::LEN <= ::core::usize::MAX / <T::Inner as IsArray>::LEN }>: True,
    [(); T::LEN * <T::Inner as IsArray>::LEN]:,
{
    chunks: ManuallyDrop<T>,
    whole: ManuallyDrop<[<T::Inner as IsArray>::Inner; T::LEN * <T::Inner as IsArray>::LEN]>,
}

impl<T> OrdesFlatten for T
where
    T: IsArray,
    T::Inner: IsArray,
    ConstCheck<{ <Self::Inner as IsArray>::LEN <= ::core::usize::MAX / Self::LEN }>: True,
    ConstCheck<{ Self::LEN <= ::core::usize::MAX / <Self::Inner as IsArray>::LEN }>: True,
    [(); Self::LEN * <Self::Inner as IsArray>::LEN]:,
{
    fn flatten(
        self,
    ) -> [<Self::Inner as IsArray>::Inner; Self::LEN * <Self::Inner as IsArray>::LEN] {
        unsafe {
            let joiner = FlattenJoiner {
                chunks: ManuallyDrop::new(self),
            };
            ManuallyDrop::into_inner(joiner.whole)
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn thing() {
        let mut iter = 0..;
        let data = [(); 18].map(|_| iter.next().unwrap());
        let (a, b): ([[u8; 2]; 9], [u8; 0]) = data.ordes_chunks::<2>();
        let tmp: [u8; 18] = a.flatten();
        assert_eq!(data, tmp);
        assert_eq!(b, []);
    }
}
