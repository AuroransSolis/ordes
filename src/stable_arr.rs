use crate::{OrdesCons, OrdesPop, OrdesPush, OrdesRest};
use ordes_macros::impl_ops_arr;
use seq_macro::seq;

seq! {N in 1..=32 {
    impl_ops_arr!(N);
}}

#[cfg(feature = "len_32_64")]
seq! {N in 33..=64 {
    impl_ops_arr!(N);
}}

#[cfg(feature = "len_64_128")]
seq! {N in 65..=128 {
    impl_ops_arr!(N);
}}

#[cfg(feature = "len_128_256")]
seq! {N in 129..=256 {
    impl_ops_arr!(N);
}}

#[cfg(feature = "len_256_512")]
seq! {N in 257..=512 {
    impl_ops_arr!(N);
}}

#[cfg(feature = "len_512_1024")]
seq! {N in 513..=1024 {
    impl_ops_arr!(N);
}}
